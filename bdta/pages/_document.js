/** @format */

import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html dir="ltr" lang="vi">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
