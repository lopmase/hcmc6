/**
 * @format
 * @type {import('next').NextConfig}
 */

const nextConfig = {
  reactStrictMode: true,
  experimental: {
    images: { allowFutureImage: true },
    urlImports: ['https://fonts.googleapis.com'],
  },
  compiler: {
    styledComponents: true,
    removeConsole: false,
  },
  images: {
    domains: ["dkgh.vj-digital.com", "localhost","https://jsonplaceholder.typicode.com"],
  },
}

module.exports = nextConfig
