import Image from 'next/future/image';
import React from 'react';
import style from '../../styles/Home.module.scss'


const Detail = () => {
    return (
        <div className={style.Detail}>
            <div className="detail_container">
                <div className="detail_info">
                    <div className="info_logo">
                        <Image
                            src={'/auu.jpg'}
                        ></Image>
                    </div>
                    <h4 className='info_name'>Võ Đức Thắng</h4>
                    <ul className="info_list">
                        <li className="info_item">

                        </li>
                    </ul>
                </div>
                <div className="detail content"></div>
            </div>
        </div>
    );
};

export default Detail;