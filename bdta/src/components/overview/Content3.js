import React, { useMemo } from 'react';
import Wrapper from '../UI/Wrapper';
import CardComponent from '../UI/CardComponent';
import style from '../../../styles/Home.module.scss';
import { CONSTANT } from '../../util/CONSTANT';

const Content3 = (props) => {
  const length = useMemo(() => {
    const length1 = CONSTANT.CONTENTCONNECT.length;
    const length2 = CONSTANT.CONTENTCOMPANION.length;
    const length3 = CONSTANT.CONTENTTREND.length;

    if (length3 <= length2 && length3 <= length1) {
      return length3;
    }
    if (length2 <= length3 && length2 <= length1) {
      return length2;
    }
    if (length1 <= length3 && length1 <= length2) {
      return length2;
    }
  }, []);

  return (
    <Wrapper wrapperStyle={props.style}>
      <h1 className={style.titleBenefit}>
        <b
          style={{
            color: '#cf2030',
            lineHeight: 1,
            textTransform: 'uppercase',
            fontWeight: 700,
          }}
        >
          NHỮNG LỢI ÍCH KHI THAM GIA{' '}
        </b>
        <b
          style={{
            lineHeight: 1,
            textTransform: 'uppercase',
            fontWeight: 700,
          }}
        >
          BNI HCM
        </b>
        <b
          style={{
            color: '#cf2030',
           
          }}
        >
          {' '}
          CENTRAL 6
        </b>
      </h1>
      <div className={style.cardGrid}>
        <div className={style.cardItem}>
          <CardComponent
            length={length}
            src='/ketnoi.jpg'
            title='KẾT NỐI KINH DOANH'
            content={CONSTANT.CONTENTCONNECT}
          />
        </div>
        <div className={style.cardItem}>
          <CardComponent
            length={length}
            src='/donghanh2.jpg'
            title='ĐỒNG HÀNH'
            content={CONSTANT.CONTENTCOMPANION}
          />
        </div>
        <div className={style.cardItem}>
          <CardComponent
            length={length}
            src='/xuthe.jpeg'
            title='ĐÓN ĐẦU XU THẾ'
            content={CONSTANT.CONTENTTREND}
          />
        </div>
      </div>
    </Wrapper>
  );
};

export default Content3;
