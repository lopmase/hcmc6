import { useEffect, useState } from 'react';
import { logoDetail, logoStand } from './data';
import Container from '../UI/Container';
import Detail from './Detail';
import Donor from './Donor';
import style from '../../../styles/Home.module.scss';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
const DonorList = () => {
  return (
    <Container ortherStyle={{ marginTop: 80}}>
      <h1 className={style.contentTitle} style={{ color: '#212529' }}>
        <span>NHÀ TÀI TRỢ</span>
        <span style={{ color: '#cf2030' }}> BUSINESS MATCHING LẦN 3</span>
      </h1>

      <div key={uuidv4()} className={style.partnerGrid}>
        {logoStand.map((item) => (
          <div key={item.logo} className={style.partnerItem}>
            <Donor item={item} />
          </div>
        ))}
      </div>

      {/* <div key={uuidv4()} className={style.partnerGrid}>
        {logoDetai1.map((item) => (
          <div key={uuidv4()} className={style.parnerItem}>
            {donor ? <Donor item={item} /> : <Detail item={item} />}
          </div>
        ))}
      </div> */}
    </Container>
  );
};

export default DonorList;
