import style from '../../../styles/Home.module.scss';
import { iframeList } from '../memberList/eventList';
import Image from 'next//future/image';
import Link from 'next/link';

const Iframe = ({ src }) => {
  return (
    <div className={style.slider}>
      {
        <div className={style.iframeList}>
          {iframeList.map((item, index) => (
            <div key={item.id} className={style.iframeItem}>
              <Link href={item.href}>
                <a>
                  <Image
                    src={item.src}
                    width={300}
                    height={150}
                    layout='responsive'
                  />
                </a>
              </Link>
            </div>
          ))}
        </div>
      }
    </div>
  );
};

export default Iframe;
