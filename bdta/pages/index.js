/** @format */
import Head from 'next/head'
import dynamic from 'next/dynamic'
import Image from 'next/image';
import Header from '../src/components/header'
import Footer from '../src/components/footer'
import Link from 'next/link'
import MemberList from '../src/components/memberList'
import OverView from '../src/components/overview'
import Subject from '../src/components/subject'
import style from '../styles/Home.module.scss'
import { positions, Provider } from 'react-alert'
import AlertMUITemplate from 'react-alert-template-mui'
import DonorList from '../src/components/details/DonorList'
import award from '../public/award.png';
import location from '../public/location.png';
import book from '../public/books-stack-of-three.png';
import Container from '../src/components/UI/Container';
// import Detail from '../src/components/detail/Detail';

const options = {
  position: positions.MIDDLE,
}
const DetailList = dynamic(
  () => import('../src/components/details/DetailList'),
  {
    ssr: false,
    suspense: true,
  }
)

const CountdownTimer = dynamic(() => import('../src/components/countdown/CountDownTimer'), {
  ssr: false,
});


export default function Home() {
  return (
    <Provider template={AlertMUITemplate} {...options}>
      <div>
        <Head>
          <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta name='title' content='BNI Hồ Chí Minh CENTRAL 6' />
          <meta
            name='description'
            content='Business Matching là hoạt động kết nối kinh doanh thực hiện xúc tiến thương mại nhằm giúp các doanh nghiệp có cơ hội phát triển và tăng việc kết nối kinh doanh với khách hàng.
            Con số dự kiến được thống kê sau BUSINESS MATCHING lần 1 và 2 thì lần kết nối sắp tới đây của lần 3 với mục tiêu "Better Together - Cùng nhau trở nên tốt hơn" sẽ có được 700 Cơ hội được trao tại chỗ và giá trị lên đến 900 Triệu'
          />
          <meta name='keywords' content='BNI Hồ Chí Minh CENTRAL 6' />
          <meta name='author' content='Viet Japan Partner' />
          <meta property='og:title' content='BNI Hồ Chí Minh CENTRAL 6 - BUSINESS MATCHING LẦN 3' />
          <meta property='og:url' content='' />
          <meta
            property='og:description'
            content='BNI - BUSINESS NETWORK INTERNATIONAL
là tổ chức kết nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay, được Tiến sĩ Ivan Misner sáng lập vào năm 1985. Cho đến nay BNI Toàn cầu đã trao cho nhau 12.2 triệu cơ hội kinh doanh (referrals) với tổng trị giá 16.9 tỉ USD'
          />
          <meta property='og:image' content='/thumbnail-bnic6.png' />
          <meta itemProp='image' content='/thumbnail-bnic6.png' />
          <meta property='og:type' content='website'></meta>
          <link href='/favicon.png' rel='icon' type='image/x-icon' />
          <title>BNI Hồ Chí Minh CENTRAL 6</title>
        </Head>

        <main>
          <Header />
          <OverView />
          <Subject />

          <div className={style.registerContainer}>
          <Container>
          <div className={style.location}>
            <div className={style.locationItem}>
              <div className={style.itemImage}>
                <Image src={award} />
              </div>

              <div className={style.detail}>
                <h3>THỜI GIAN</h3>
                <p>07h00 – 17h00, Thứ Năm, 11/08/2022</p>
                <p>07h00 – 21h00, Thứ Sáu, 12/08/2022</p>
              </div>
            </div>
            <div className={style.locationItem}>
              <div className={style.itemImage}>
                <Image src={location} />
              </div>
              <div className={style.detail}>
                <h3>ĐỊA ĐIỂM</h3>
                <p>
                  Trung tâm hội nghị ADORA Nguyễn Kiệm,
                  TP. Hồ Chí Minh
                </p>
              </div>
            </div>
            <div className={style.locationItem}>
              <div className={style.itemImage}>
                <Image src={book} />
              </div>

              <div className={style.detail}>
                <h3>Facebook Event</h3>
                <p>Business Matching HCM6 lần thứ 3</p>
              </div>
            </div>
          </div>
          <CountdownTimer />
        </Container>
            <Link href='#form'>
              <a className={style.btnSingle}>ĐĂNG KÝ THAM GIA</a>
            </Link>
          </div>
          <MemberList />
          <DonorList />
          <DetailList donor={false} />
        </main>

        <Footer />
      </div>
    </Provider>
  )
}
