import React, { useEffect, useState } from 'react';
import Container from '../UI/Container';
import style from '../../../styles/Home.module.scss';
import axios from 'axios';
import { useAlert } from 'react-alert';
import CustomSelect from './components/CustomSelect';
import { faListSquares } from '@fortawesome/free-solid-svg-icons';
import OverPlay from './components/OverPlay';
import { Select } from 'antd';
const { Option } = Select;

const initialForm = {
  fullName: '',
  phoneNumber: '',
  email: '',
  companyName: '',
  position: '',
  gender: 'male',
  media: '',
  investor: '',
  chapter: '',
};

const Form = () => {
  const alert = useAlert();
  const [formRegister, setFormRegister] = useState(initialForm);
  const [listChapter, setListChapter] = useState([]);
  const [loading, setLoading] = useState(false);

  // const [checkPhone, setCheckPhone] = useState(false)
  const [check, setCheck] = useState({
    male: true,
    female: false,
  });
  useEffect(() => {
    const fetchChapter = async () => {
      try {
        const res = await axios.get(
          'https://dkgh.vj-digital.com/api/v1/loadChapter'
        );
        setListChapter(res.data.data);
      } catch (err) {}
    };
    fetchChapter();
  }, []);

  const changeHandler = (value, name) => {
    setFormRegister((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleChangeSelect = (e) => {
    setFormRegister((prev) => {
      return {
        ...prev,
        media: e.target.value,
      };
    });
  };
  const handleChangeCheckbox = (e) => {
    const { name, value } = e.target;
    if (name === 'male') {
      setCheck({
        male: true,
        female: false,
      });
      setFormRegister((prev) => {
        return {
          ...prev,
          gender: value,
        };
      });
      return;
    }

    setCheck({
      male: false,
      female: true,
    });
    setFormRegister((prev) => {
      return {
        ...prev,
        gender: value,
      };
    });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    // if(name === 'phoneNumber') {
    //     setCheckPhone(false)
    // }
    setFormRegister((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const saveInfo = async (data) => {
    try {
      const res = await axios.post(
        'https://dkgh.vj-digital.com/api/v1/saveFormEvent',
        {
          withCredentials: true,
          crossDomain: true,
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
          data,
        }
      );
      if (res.status == 200) {
        alert.show('Cảm ơn bạn đã đăng ký tham gia sự kiện', {
          title: 'Bạn đã đăng ký thành công',
          closeCopy: 'Đóng',
        });
        setLoading(false);
      }
    } catch (err) {
      alert.show('Đăng ký thất bại', {
        title: 'Fail',
        closeCopy: 'Đóng',
      });
      setLoading(false);
    }
  };

  const handeOnsubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    saveInfo(formRegister);
    setFormRegister(initialForm);
  };

  return (
    <div id='form' className={style.formBg}>
      <Container ortherStyle={{ width: '60vw' }}>
        <div className={style.contact1}>
          <div className={style.containerContact1}>
            <form
              onSubmit={handeOnsubmit}  
              className={style.contact1Form}
            >
              <span className={style.contact1FormTitle}>
                FORM ĐĂNG KÝ THAM GIA SỰ KIỆN
              </span>
              <div className={style.wrapInput1}>
                <div>
                  <input
                    className={`${style.input1} ${style.validateInput}`}
                    value={formRegister.fullName}
                    required
                    name='fullName'
                    placeholder='Họ và tên (bắt buộc)'
                    onChange={handleInputChange}
                  ></input>
                </div>
                <div style={{ margin: 'auto 0' }}>
                  <div className='pretty p-icon p-round p-jelly'>
                    <input
                      type='checkbox'
                      name='male'
                      value='male'
                      checked={check.male}
                      onChange={handleChangeCheckbox}
                    />
                    <div className='state p-primary'>
                      <label htmlFor='male'>Nam</label>
                    </div>
                  </div>
                  <div className='pretty p-icon p-round p-jelly'>
                    <input
                      type='checkbox'
                      name='female'
                      value='female'
                      checked={check.female}
                      onChange={handleChangeCheckbox}
                    />
                    <div className='state p-primary'>
                      <label htmlFor='female'>Nữ</label>
                    </div>
                  </div>
                </div>
              </div>
              <div className={style.wrapInput1}>
                <div>
                  <input
                    className={`${style.input1} ${style.validateInput}`}
                    required
                    type='text'
                    value={formRegister.phoneNumber}
                    name='phoneNumber'
                    placeholder='Số điện thoại (bắt buộc)'
                    onChange={handleInputChange}
                    onKeyPress={(e) => {
                      if (!/[0-9]/.test(e.key)) {
                        e.preventDefault();
                      }
                    }}
                  />
                  {/* {checkPhone && <p style={{color:'red'}}>Số điện thoại không hợp lệ (10 chữ số)</p> } */}
                </div>
                <div>
                  <input
                    className={`${style.input1} ${style.validateInput}`}
                    required
                    type='email'
                    value={formRegister.email}
                    name='email'
                    placeholder='Email (bắt buộc)'
                    onChange={handleInputChange}
                  />
                </div>
              </div>

              <div className={style.wrapInput1}>
                <div>
                  <input
                    className={`${style.input1} ${style.validateInput}`}
                    required
                    value={formRegister.position}
                    name='position'
                    placeholder='Chức vụ (bắt buộc)'
                    onChange={handleInputChange}
                  />
                </div>
                <div>
                  <input
                    className={`${style.input1} ${style.validateInput}`}
                    required
                    type='text'
                    value={formRegister.companyName}
                    name='companyName'
                    placeholder='Tên doanh nghiệp (bắt buộc)'
                    onChange={handleInputChange}
                  />
                </div>
              </div>
              <div className={style.wrapInput2}>
                <select
                  className={style.selectForm}
                  defaultValue={formRegister.media}
                  name='media'
                  onChange={handleChangeSelect}
                >
                  {formRegister.media === '' && (
                    <option
                      className={style.optionForm}
                      value=''
                      disabled
                      selected
                    >
                      Bạn biết thông tin này qua đâu
                    </option>
                  )}
                  <option value='Zalo'>Zalo</option>
                  <option value='FaceBook'>FaceBook</option>
                  <option value='friend'>Bạn bè</option>
                  <option value='other'>Khác</option>
                </select>
              </div>

              {formRegister.media === 'friend' && (
                <div className={style.wrapInput1}>
                  <div>
                    <input
                      className={`${style.input1} ${style.validateInput}`}
                      value={formRegister.investor}
                      name='investor'
                      placeholder='Bạn là khách mời của ai ?'
                      onChange={handleInputChange}
                    />
                  </div>
                  <div>
                    <CustomSelect
                      searchPlaceholder='Search'
                      data={listChapter}
                      value={formRegister.chapter}
                      onChange={changeHandler}
                      name='chapter'
                    />
               
                  </div>
                </div>
              )}

              <div className={style.containerContact1FormBtn}>
                <button type='submit' className={style.contact1FormBtn}>
                  <span>
                    Đăng ký
                    <i
                      className='fa fa-long-arrow-right'
                      aria-hidden='true'
                    ></i>
                  </span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </Container>
      {loading && (
        <div className={style.OverPlayContainer}>
          <OverPlay loading={loading} />
        </div>
      )}
    </div>
  );
};

export default Form;
