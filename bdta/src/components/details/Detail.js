import React, { useState, useEffect } from 'react'
import Modal from './Modal'
import style from '../../../styles/Home.module.scss'
import Image from 'next/future/image'
import dynamic from 'next/dynamic'
import Lightbox from "react-awesome-lightbox";
//import ImageLightbox from '../lightBox/LightBox'
const Detail = ({ item }) => {
  const [show, setShow] = useState(false)
  const [selectedIndex, setSelectedId] = useState(null)
  const [lightboxVisible, setLightboxVisible] = useState(false)


  return (
    <>
      <Image
        onClick={() => setShow(true)}
        className={style.logoDonors}
        src={item.logoImage}
        // layout='responsive'
        width={520}
        height={250}
        alt='detail'
        style={{ width: '100%', height: '100%', objectFit: 'contain' }}
      />
      {show && <Modal setLightboxVisible={setLightboxVisible} setSelectedId={setSelectedId} item={item} show={show} onClose={() => setShow(false)} />}
      {lightboxVisible && <Lightbox onClose={() => setLightboxVisible(false)} image={item.imgServices[selectedIndex]}></Lightbox>}
    </>
  )
}

export default Detail
