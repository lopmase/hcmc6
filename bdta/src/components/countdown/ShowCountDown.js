/** @format */

import { motion } from 'framer-motion'
import style from '../../../styles/Home.module.scss'

const ShowCountDown = ({ days, hours, minutes, seconds }) => {
  return (
    <div className={style.countDownContainer}>

      <div className={style.countdownItem} style={{ fontSize: 50 }}
      >
        <motion.h1
          style={{ color: 'black' }}
          initial={{ y: -150, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          exit={{ y: 75, opacity: 0, position: "absolute" }}
          transition={{
            ease: "easeIn",
            duration: 1,
          }}
        >{days}</motion.h1>
        <div className={style.date} >
          Ngày
        </div>
      </div>
      <div className={style.countdownItem} style={{ fontSize: 50 }}>
        <motion.h1
          style={{ color: 'black' }}
          initial={{ y: -150, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          exit={{ y: 75, opacity: 0, position: "absolute" }}
          transition={{
            ease: "easeOut",
            duration: 1,
          }}
        >{hours}</motion.h1>
        <div className={style.date}>
          Giờ
        </div>
      </div>
      <div className={style.countdownItem} style={{ fontSize: 50 }}>
        <motion.h1
          style={{ color: 'black' }}
          initial={{ y: -150, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          exit={{ y: 75, opacity: 0, position: "absolute" }}
          transition={{
            ease: "easeOut",
            duration: 1,
          }}
        >{minutes}</motion.h1>
        <div className={style.date}>
          Phút
        </div>
      </div>
      <div className={style.countdownItem} style={{ fontSize: 50 }}>
        <motion.h1
          style={{ color: 'black'}}
          initial={{ y: -150, opacity: 0}}
          animate={{ y: 0, opacity: 1}}
          exit={{ y: 75, opacity: 0, position: "absolute" }}
          transition={{
            ease: "easeOut",
            duration: 1,
          }}
      
          
        >{seconds}</motion.h1>
        <div className={style.date}>
          Giây
        </div>
      </div>
    </div>
  )
}

export default ShowCountDown
