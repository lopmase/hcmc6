import React, { useEffect, useRef, useState } from 'react';
import style from '../../../styles/Home.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPhone,
  faEnvelope,
  faGlobe,
  faLocationDot,
} from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Image from 'next/image';
import close from '../../../public/close.png';
import { v4 as uuidv4 } from 'uuid';
import { AnimatePresence, AnimateSharedLayout } from "framer-motion"
import Lightbox from "react-awesome-lightbox";
import { motion } from "framer-motion"
import "react-awesome-lightbox/build/style.css";

const Modal = ({ item, modalStyle, show, onClose, backdropStyle, setLightboxVisible, setSelectedId }) => {

  const modalRef = useRef(null);
 
  const { business } = item

  useEffect(() => {
    if (show) {
      modalRef.current.classList.add(style.visible);
    } else {
      modalRef.current.classList.remove(style.visible);
    }

  }, [show]);


  return (
    <React.Fragment>
      <div
        ref={modalRef}
        style={backdropStyle}
        className={`${style.modal__wrap}`}
        onClick={onClose}
      >
        <div
          style={modalStyle}
          className={style.modal}
          onClick={(e) => e.stopPropagation()}
        >
          <div className={style.modalGrid}>

            <div className={style.modalClose} onClick={onClose}>
              <Image src={close} alt='close' />
            </div>

            <div
              className={style.modalItem}
              style={{
                backgroundColor: '#c8102e',

              }}
            >
              <div className={style.detailInfoLeft}>
                <div className={style.membersImage}>
                  <Image
                    // layout='responsive'
                    width={450}
                    height={250}
                    src={item.logoImage}
                    alt=''
                  />
                </div>
                <div className={style.infoContent}>
                  <h4 className={style.membersName}>{item.fullName}</h4>
                  <p className={style.infoText}>{item.position}</p>
                  <div className={style.infoDetail}>
                    <FontAwesomeIcon icon={faPhone} />
                    <p>{item.phone}</p>
                  </div>

                  <div className={style.infoDetail}>
                    <FontAwesomeIcon icon={faEnvelope} />
                    <p>{item.email}</p>
                  </div>

                  <div className={style.infoDetail}>
                    <FontAwesomeIcon icon={faGlobe} />
                    <Link href='#'>
                      <a style={{ color: 'white', fontSize: 18 }}>
                        {item.linkWeb}
                      </a>
                    </Link>
                  </div>

                  <div className={style.infoDetail}>
                    <FontAwesomeIcon icon={faLocationDot} />
                    <p>{item.address}</p>
                  </div>

                </div>
                <Link href={`https://bm3.bnicbd.com/detail/${item.id}`} >
                  <a className={style.infoButton}>Chi tiết gian hàng</a>
                </Link>
              </div>
            </div>
            <div
              div
              className={style.modalItem}
              style={{
                backgroundColor: '#f1f0f0',
              }}
            >
              <div className={style.detailInfoRight}>
                <h3>Gian hàng: đang cập nhật</h3>
                <h4>{item.companyName}</h4>
                <p>{item.experience}</p>
                <p>
                  {' '}
                  Ngành nghề độc quyền{' '}
                  <b style={{ color: '#D74653', textTransform: 'uppercase' }}>
                    {item.career}
                  </b>
                </p>
                <div dangerouslySetInnerHTML={{ __html: business }}></div>
                <p>{item.purpose}</p>
                <div className={style.lineModal}></div>
                <div className={style.productCompany}>
                  <h4>Sản phẩm / dịch vụ</h4>
                  <div className={style.serviceGrid}>
                    {item.imgServices &&
                      item.imgServices.length > 0 &&
                      item.imgServices.map((product, index) => (
                        <motion.div

                          className={style.serviceItem}
                          key={uuidv4()}

                          onClick={() => {
                            setSelectedId(index);
                            setLightboxVisible(true)
                          }}
                        >
                          <Image
                            // layout='responsive'
                            width={600}
                            height={350}
                            src={product}
                            alt=''
                          />
                        </motion.div>
                      ))}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Modal;
