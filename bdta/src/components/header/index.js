/** @format */
import style from '../../../styles/Home.module.scss'
import Image from 'next/future/image'
import banner from '../../../public/posterNew.jpg'
const index = () => {
  return <div className={style.headerBanner} alt="banner" >
    <Image
    className={style.header}
    alt="banner" 
    src={banner} />
  </div>


}

export default index
