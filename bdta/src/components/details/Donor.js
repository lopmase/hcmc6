import React from 'react'
import style from '../../../styles/Home.module.scss'
import Image from 'next/future/image'

const Donor = ({ item }) => {
  return (
      <Image
        // layout='responsive'
        width={500}
        height={250}
        src={item.logo}
        className={style.logoDonors}
        alt='donor'
        style={{width: '100%', height: '100%', objectFit: 'contain'}}
      />
  )
}

export default Donor
