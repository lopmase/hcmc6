/** @format */

import React, { useState } from 'react'
import { useCountDown } from './useCountDown'
import ShowCountDown from './ShowCountDown'



const CountdownTimer = () => {
  // 30/7 hạn đk
  const targetDate = new Date(2022, 7, 11, 6, 0, 0, 0).getTime()
  const [days, hours, minutes, seconds] = useCountDown(targetDate)
    return (
      <>
        <ShowCountDown
          days={days}
          hours={hours}
          minutes={minutes}
          seconds={seconds}
        />
      </>
    )
}

export default CountdownTimer
