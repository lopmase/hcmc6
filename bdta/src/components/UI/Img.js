/** @format */
import { useEffect, useRef } from 'react'
import propsType from 'prop-types'
import { motion, useAnimation, useInView } from 'framer-motion'

import style from '../../../styles/Home.module.scss'

const Img = ({ src, animation }) => {
  const ref = useRef(null)
  const isInView = useInView(ref)
  const controls = useAnimation()
  useEffect(() => {

    if (isInView) {

      controls.start({ x: 0, opacity: 1, rotate: 360 })
    }
  }, [isInView, controls])
  return (
    <>
      {animation ? (
        <motion.img
          ref={ref}
          initial="hidden"
          animate={controls}
          // animate={controls}
          transition={{ delay: 0.5 }}
          src={src}
          style={{ borderRadius: 10 }}
        />
      ) : (
        <img src={src} className={style.img50} />
      )}
    </>
  )
}

// Img.propTypes = {
//   src: PropTypes.string,
//   animation: PropTypes.bool,
// }

Img.defaultProps = {
  src: '/LOGO_VJD.png',
  animation: false,
}
export default Img
// <img src={src} className={style.img50} />
