import style from '../../../styles/Home.module.scss';
import { eventList } from '../memberList/eventList';

import Image from 'next/future/image';
import { v4 as uuidv4 } from 'uuid';

const ImageList = () => {
  return (
    <div className={style.slider}>
      <h1>
        {' '}
        <span>HÌNH ẢNH SỰ KIỆN</span> ĐƯỢC TỔ CHỨC LẦN 1 - 2
      </h1>

      <div className={style.imageList}>
        {eventList.map((c) => (
          <div key={c.id}>
            <Image
              width={300}
              height={150}
              layout='responsive'
              className={style.imageSlider}
              key={c.src}
              src={c.src}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ImageList;
