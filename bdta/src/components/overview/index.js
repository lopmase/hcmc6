/** @format */
import style from '../../../styles/Home.module.scss';
import Container from '../UI/Container';
import Wrapper from '../UI/Wrapper';
import Line from '../UI/Line';
import Image from 'next/future/image';
import Button from '../UI/Button';
import Title from '../UI/Title';
import {
  statisticalList,
  statisticalListGlobal,
} from '../statistical/statisticalList';
import ListStatistical from '../statistical/ListStatistical';
import 'swiper/css/navigation';
import poster from '../../../public/poster.jpeg';
import dynamic from 'next/dynamic';


const Iframe = dynamic(() => import('../UI/Iframe'), {
  ssr: false,
})


const ImageList = dynamic(() => import('../UI/ImageList'), {
  ssr: false,
});

const OverView = () => {
  return (
    <Container>
      <Line style={{ marginTop: 50, backgroundColor: 'grey', height: 2 }} />
      <Image
        // layout='responsive'
        width={280}
        height={200}
        className={style.logoBNI}
        src='/logoNew.png'
        alt='logo'
      />

      <div
        style={{
          margin: 'auto',
          width: '100%',
          textAlign: 'center',
          padding: 15,
        }}
      >
        <Title style={{ marginBottom: 10, fontSize: 23, fontWeight: 700, color:'black' }}>
          CHÀO MỪNG ĐẾN VỚI BNI
        </Title>

        <p className={style.text}>
          Bạn có biết <span style={{ color: '#D03031' }}>BNI</span> - là tổ chức kết
          nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay.
        </p>
      </div>
      <ListStatistical
        list={statisticalListGlobal}
        src='https://www.bni.com/'
        title='BNI toàn cầu'
      />

      <ListStatistical
        list={statisticalList}
        src='https://bni.vn/'
        title='BNI Việt Nam'
      />

      <div style={{paddingLeft: 21, paddingRight: 21}} className='content_info'>
        <Title>
          <span style={{ color: '#cf2030', fontWeight: 700 }}>BNI</span> HCM CENTRAL 6
        </Title>

        <p style={{textAlign: 'justify'}} className={style.text}>
          BNI Việt Nam có 10 vùng, riêng vùng HCMC6 nằm trong{' '}
          <span style={{ color: '#cf2030', fontWeight: 700 }}>
            Top 10 vùng phát triển tốt nhất trong hệ thống BNI toàn cầu
          </span>{' '}
          do bà <span style={{ fontWeight: 700 }}>Anna Nguyễn Thị Bích Hằng </span>
          điều hành với <span style={{ fontWeight: 700 }}>08 Chapter</span> cùng{' '}
          <span style={{ fontWeight: 700 }}>hơn 500 thành viên</span> Chủ doanh nghiệp
        </p>
      </div>


      <Wrapper wrapperStyle={{ margin: 'auto' }}></Wrapper>
      {/* BM lan 3  */}

      <Wrapper>
        <h1 className={style.contentTitle}>
          BUSINESS MATCHING
          <span> ĐƯỢC TỔ CHỨC 06 THÁNG/LẦN</span>
        </h1>
        <p style={{textAlign: 'justify'}} className={style.text}>Business Matching <span style={{ color: '#cf2030', fontWeight: 700 }}>là hoạt động kết nối kinh doanh</span> thực hiện xúc tiến thương mại nhằm giúp các doanh nghiệp có cơ hội phát triển và tăng việc kết nối kinh doanh với khách hàng. 
         <br/>Con số dự kiến được thống kê sau BUSINESS MATCHING lần 1 và 2 thì lần kết nối sắp tới đây của lần 3 với mục tiêu <span style={{ color: '#cf2030', fontWeight: 700 }}>	&quot;Better Together - Cùng nhau trở nên tốt hơn&quot; </span> sẽ có được <span>700 Cơ hội</span> được trao tại chỗ và giá trị lên đến <span>900 Triệu</span></p>
        <h1 className={style.titlePoster}><span style={{textTransform:'none'}}>Chủ đề:</span> BUSINESS MATCHING - BNI BETTER TOGETHER DAYS</h1>
        <div className={style.posterContainer}>
          <Image layout='responsive' className={style.poster} src={poster} />
        </div>
        {/* <div className={style.posterContainer}>
          <Image layout='responsive' className={style.poster} src={poster} />
        </div> */}
      </Wrapper>
      <ImageList />
      <Iframe/>
      <Button style={{ position: 'fixed', bottom: 30, right: 20 }} href='#form'>
        ĐĂNG KÝ NGAY
      </Button>
    </Container>
  );
};

export default OverView;
