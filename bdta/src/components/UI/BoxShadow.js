

const BoxShadow = (props) => {
  return (
    <div style={{width: '100%',borderRadius: 10, boxShadow: '10px 10px rgba(230, 229, 229, 1)'}}>
        {props.children}
    </div>
  )
}

export default BoxShadow
